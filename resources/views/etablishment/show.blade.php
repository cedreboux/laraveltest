@extends ('layouts.app')

@section('content')
<div class="w-4/5 m-auto text-left">
    <div class="py-15">
        <h1 class="text-5xl font-extrabold">{{$post->name}}</h1>
    </div>
</div>
<div class="w-4/5 m-auto pt-20">
    <div>
        <img src="{{asset('images/'.$post->image_path)}}">
    </div>
    <p class="text-xl text-gray-700 pt-8 pb-10 leading-8 font-light">{{$post->address}}</p>
    <p class="text-xl text-gray-700 pt-8 pb-10 leading-8 font-light">{{$post->city}} {{$post->zip_code}}, {{$post->country}}</p>
    <span class="text-gray-500">
        Par <span class="font-bold italic text-gray-800">{{$post->user->name}}</span> crée le {{date('jS M Y', strtotime($post->updated_at))}}
    </span>
</div>

@endsection
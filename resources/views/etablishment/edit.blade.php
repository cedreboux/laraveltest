@extends ('layouts.app')

@section('content')
<div class="w-4/5 m-auto text-left">
    <div class="py-15">
        <h1 class="text-5xl font-extrabold">Mettre à jour un établissement</h1>
    </div>
</div>

@if($errors->any())
    <div class="w-4/5 m-auto">
        <ul>
            @foreach($errors->all() as $error)
                <li class="w-1/5 text-gray-50 bg-red-500 rounded-2xl py-4">{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="w-4/5 m-auto pt-20">
    <form action="/place/{{$post->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <input type="text" name="name" value="{{$post->name}}" class="bg-transparent block border-b-2 w-full h-20 text-6xl outline-none"/>
        <input type="text" name="address" value="{{$post->address}}" class="bg-gray block border-b-2 w-full h-20 text-2xl outline-none"/>
        <input type="text" name="city" value="{{$post->city}}" class="bg-gray block border-b-2 w-full h-20 text-2xl outline-none"/>
        <input type="text" name="zip_code" value="{{$post->zip_code}}" class="bg-gray block border-b-2 w-full h-20 text-2xl outline-none"/>
        <input type="text" name="country" value="{{$post->country}}" class="bg-gray block border-b-2 w-full h-20 text-2xl outline-none"/>
        <button type="submit" class="uppercase mt-15  bg-blue-500 text-gray-100 text-lg font-extrabold py-4 px-8 rounded-3xl">Mettre à jour</button>
    </form>
</div>
@endsection
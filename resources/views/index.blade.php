@extends('layouts.app')

@section('content')
    <div class="grid grid-cols-1 m-auto" style="background-image: url('https://cdn.pixabay.com/photo/2017/01/13/08/08/tori-1976609_1280.jpg');background-repeat: no-repeat;background-position: center center;background-size: cover;height: 600px;">
        <div class="flex text-gray-100 pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block-text-center">
                <h1 class="sm:text-black text-5xl uppercase font-bold text-shadow-md pb-14">Tori</h1>
                <a href="/place" class="text-center bg-red-400 text-gray-700 py-2 px-4 font-bold text-xl uppercase">Lire</a>
            </div>
        </div>
    </div>
    <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
        <div>
            <img src="https://cdn.pixabay.com/photo/2015/02/15/03/04/japanese-umbrellas-636870_1280.jpg" width="500" alt="">
        </div>
        <div class="m-auto sm:m-auto text-left w-4/5 block">
            <h2 class="text-4xl font-extrabold text-gray-600">Parapluies japonais</h2>
            <p class="py-8 text-gray-500 text-l">Boutique à visiter</p>
            <p class="font-extrabold text-gray-600 text-l pb-9"></p>
            <a href="/place" class="uppercase bg-blue-500 text-gray-600 font-extrabold py-3 px-4 rounded-3xl">En savoir plus</a>
        </div>
    </div>
    <div class="text-center p-15 bg-black text-white">
        <h2 class="text-2xl pb-5 text-l">Découvrez</h2>
        <span class="font-extrabold block text-4xl py-1">Restaurants</span>
        <span class="font-extrabold block text-4xl py-1">Expo</span>
        <span class="font-extrabold block text-4xl py-1">Magasins</span>
        <span class="font-extrabold block text-4xl py-1">Tout connaître</span>
    </div>
    <div class="text-center py-15">
        <span class="uppercase text-s text-gray-400">Prévoyez ce week-ends</span>
        <h2 class="text-4xl font-bold py-10">Lieu récent</h2>
        <div class="flex w-4/5 m-auto">
        @foreach ($posts as $post)
            <div class="flex-3">
                <a href="/place/{{$post->id}}"><img class="w-2/6 m-auto pb-5" src="{{asset('images/'.$post->image_path)}}"></a>
                <div>
                    <a href="/place/{{$post->id}}" class="block text-lg pb-5 text-l">{{$post->name}}</a>
                    <a href="/place/{{$post->id}}" class="block text-lg pb-5 text-l">{{$post->city}} {{$post->zip_code}}, {{$post->country}}</a>
                </div>
            </div>
        @endforeach
        </div>
    </div>
@endsection
<footer class="bg-gray-800 py-20 mt-20">
    <div class="sm:grid grid-cols-3 w-4/5 pb-10 m-auto border-b-2 border-gray-700">
        <div>
            <h3 class="text-l sm:font-bold text-gray-100">
                Pages
            </h3>
            <ul class="py-4 sm:text-s pt-4 text-gray-400">
                <li class="pb-1">
                    <a href="/">Accueil</a>
                </li>
                <li class="pb-1">
                    <a href="/place">Place</a>
                </li>
                <li class="pb-1">
                    <a href="/login">Connexion</a>
                </li>
                <li class="pb-1">
                    <a href="/register">Inscription</a>
                </li>
            </ul>
        </div>

        <div>
            <h3 class="text-l sm:font-bold text-gray-100">
                Trouvez-nous
            </h3>
            <ul class="py-4 sm:text-s pt-4 text-gray-400">
                <li class="pb-1">
                    <a href="/">Ce qu'on fait</a>
                </li>
                <li class="pb-1">
                    <a href="/place">Adresse</a>
                </li>
                <li class="pb-1">
                    <a href="/login">Tél</a>
                </li>
                <li class="pb-1">
                    <a href="/register">Contact</a>
                </li>
            </ul>
        </div>
        <div>
            <h3 class="text-l sm:font-bold text-gray-100">
                Les établissements
            </h3>
            <ul class="py-4 sm:text-s pt-4 text-gray-400">
                <li class="pb-1">
                    <a href="/place">Viens voir</a>
                </li>
            </ul>
        </div>
    </div>
    <p class="w-25 w-4/5 pb-3 m-auto text-xs text-gray-100 pt-6">Copyright. Tout droit réserver.</p>
</footer>